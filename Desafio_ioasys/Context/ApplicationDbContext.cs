﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Desafio_ioasys.Entities;
using Desafio_ioasys.Migrations;

namespace Desafio_ioasys.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Seed();
        }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<Administrador> Administradores { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Filme> Filmes { get; set; }
        public DbSet<FilmeAtor> FilmesAtor { get; set; }
        public DbSet<FilmeAvaliacao> FilmesAvaliacao { get; set; }
        public DbSet<Ator> Atores { get; set; }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Enterprise>()
        //        .HasKey(p => p.Id);
        //}

    }
}
