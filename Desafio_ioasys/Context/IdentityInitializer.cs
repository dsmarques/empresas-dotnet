﻿using System;
using Desafio_ioasys.Entities;
using Desafio_ioasys.Security;
using Microsoft.AspNetCore.Identity;

namespace Desafio_ioasys.Context
{
    public class IdentityInitializer
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public IdentityInitializer(
            ApplicationDbContext context,
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public void Initialize()
        {
            if (_context.Database.EnsureCreated())
            {
                if (!_roleManager.RoleExistsAsync(Roles.ROLE_API).Result)
                {
                    var resultado = _roleManager.CreateAsync(
                        new IdentityRole(Roles.ROLE_API)).Result;
                    if (!resultado.Succeeded)
                    {
                        throw new Exception(
                            $"Erro durante a criação da role {Roles.ROLE_API}.");
                    }
                }

                //CreateUser(
                //    new ApplicationUser()
                //    {
                //        UserName = "ioasys",
                //        Email = "testeapple@ioasys.com.br",
                //        NormalizedEmail = "testeapple@ioasys.com.br",
                //        EmailConfirmed = true
                //    }, "Uioasys12341234!", Roles.ROLE_API);

                //CreateUser(
                //    new ApplicationUser()
                //    {
                //        UserName = "admin_apiprodutos",
                //        Email = "admin-apiprodutos@teste.com.br",
                //        NormalizedEmail = "admin-apiprodutos@teste.com.br",
                //        EmailConfirmed = true,
                //    }, "AdminAPIProdutos01!", Roles.ROLE_API);

                //CreateEnterprise();

            }
        }

        //private void CreateUser(
        //    ApplicationUser user,
        //    string password,
        //    string initialRole = null)
        //{
        //    if (_userManager.FindByNameAsync(user.UserName).Result == null)
        //    {
        //        var resultado = _userManager.CreateAsync(user, password).Result;

        //        if (resultado.Succeeded &&
        //            !String.IsNullOrWhiteSpace(initialRole))
        //        {
        //            _userManager.AddToRoleAsync(user, initialRole).Wait();
        //        }
        //    }
        //}

        //private void CreateEnterprise()
        //{

        //    var enterprise1 = new Enterprise
        //    {
        //        Id = 1,
        //        Nome = "DeF Desenvolvimento",
        //        Telefone = "31999851055",
        //        Endereco = "Rua Dr. Teles",
        //        Tipo = EnterpriseType.LTDA
        //    };
        //    _dbContext.Enterprises.Add(enterprise1);

        //    var enterprise2 = new Enterprise
        //    {
        //        Id = 2,
        //        Nome = "ioasys",
        //        Telefone = "31999999999",
        //        Endereco = "Belo Horizonte",
        //        Tipo = EnterpriseType.LTDA
        //    };
        //    _dbContext.Enterprises.Add(enterprise2);

        //    var enterprise3 = new Enterprise
        //    {
        //        Id = 3,
        //        Nome = "Empresa 3",
        //        Telefone = "31999998888",
        //        Endereco = "Belo Horizonte",
        //        Tipo = EnterpriseType.SociedadeAnonima
        //    };
        //    _dbContext.Enterprises.Add(enterprise3);

        //    _dbContext.SaveChanges();

        //}
    }
}
