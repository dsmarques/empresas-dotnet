﻿using System;
using Desafio_ioasys.Entities;

namespace Desafio_ioasys.Repositories.Interfaces
{
    public interface IFilmeRepository : IBaseRepository<Filme>
    {

    }
}
