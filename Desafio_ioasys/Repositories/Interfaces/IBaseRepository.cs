﻿using System;
using System.Linq;

namespace Desafio_ioasys.Repositories.Interfaces
{
    public interface IBaseRepository<T> where T : class
    {
        IQueryable<T> List { get; }
        T Find(int key);
        void Add(params T[] obj);
        void Edit(params T[] obj);
        void Remove(params T[] obj);
    }
}