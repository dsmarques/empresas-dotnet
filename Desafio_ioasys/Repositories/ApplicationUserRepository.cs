﻿using System;
using Desafio_ioasys.Context;
using Desafio_ioasys.Entities;
using Desafio_ioasys.Repositories.Interfaces;

namespace Desafio_ioasys.Repositories
{
    public class ApplicationUserRepository : BaseRepository<ApplicationUser>, IApplicationUserRepository
    {
        public ApplicationUserRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
