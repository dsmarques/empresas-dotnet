﻿using System;
using System.Linq;
using Desafio_ioasys.Context;
using Desafio_ioasys.Repositories.Interfaces;

namespace Desafio_ioasys.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        private ApplicationDbContext _context;

        public BaseRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IQueryable<T> List => _context.Set<T>().AsQueryable();

        public void Edit(params T[] obj)
        {
            _context.Set<T>().UpdateRange(obj);
            _context.SaveChanges();
        }

        public void Remove(params T[] obj)
        {
            _context.Set<T>().RemoveRange(obj);
            _context.SaveChanges();
        }

        public T Find(int key)
        {
            return _context.Find<T>(key);
        }

        public void Add(params T[] obj)
        {
            _context.Set<T>().AddRange(obj);
            _context.SaveChanges();
        }
    }
}