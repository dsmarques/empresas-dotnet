﻿using System;
using Desafio_ioasys.Context;
using Desafio_ioasys.Entities;
using Desafio_ioasys.Repositories.Interfaces;

namespace Desafio_ioasys.Repositories
{
    public class UsuarioRepository : BaseRepository<Usuario>, IUsuarioRepository
    {
        public UsuarioRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
