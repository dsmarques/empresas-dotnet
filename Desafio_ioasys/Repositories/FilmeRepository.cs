﻿using System;
using Desafio_ioasys.Context;
using Desafio_ioasys.Entities;
using Desafio_ioasys.Repositories.Interfaces;

namespace Desafio_ioasys.Repositories
{
    public class FilmeRepository : BaseRepository<Filme>, IFilmeRepository
    {
        public FilmeRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
