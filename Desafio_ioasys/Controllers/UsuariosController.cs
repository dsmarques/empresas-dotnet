﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Desafio_ioasys.Entities;
using Desafio_ioasys.Models;
using Desafio_ioasys.Repositories;
using Desafio_ioasys.Repositories.Interfaces;
using Desafio_ioasys.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Desafio_ioasys.Controllers
{

    [ApiController]
    [ApiVersion("1")]
    [Authorize("Bearer")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class UsuariosController : Controller
    {

        private IApplicationUserService IApplicationUserService;

        public UsuariosController(IApplicationUserService service)
        {
            IApplicationUserService = service;
        }

        [SwaggerOperation(
            Summary = "Recupera uma coleção dos usuários.",
            Tags = new[] { "Usuario" }
        )]
        [HttpGet]
        [ProducesResponseType(statusCode: 200, Type = typeof(List<ApplicationUserModel>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponseModel))]
        [Route("")]
        [Route("{id?}")]
        public IActionResult Get(int? id)
        {
            var lista = IApplicationUserService.List().Where(u => u.Role == EntitiesModels.Enum.UserRoleEnum.Usuario && (!id.HasValue || id.Value == u.Id)).ToList();

            List<ApplicationUserModel> users = new List<ApplicationUserModel>();

            foreach (var item in lista)
            {
                users.Add(IApplicationUserService.ToModel(item));
            }

            return Ok(users);
        }

        [SwaggerOperation(
            Summary = "Processa a exclusão logica de um usuário.",
            Tags = new[] { "Usuario" }
        )]
        [HttpDelete]
        [ProducesResponseType(statusCode: 200, Type = typeof(ApplicationUserModel))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponseModel))]
        public IActionResult Delete([FromBody] ApplicationUserModel model)
        {
            var user = IApplicationUserService.Find(model.Id);
            if (user != null)
            {
                user.Ativo = false;
                IApplicationUserService.Edit(user);
            } else
            {
                return NotFound();
            }

            return Ok(user);
        }


        [SwaggerOperation(
            Summary = "Faz inclusão de usuario.",
            Tags = new[] { "Usuario" }
        )]
        [HttpPost]
        [ProducesResponseType(statusCode: 200, Type = typeof(ApplicationUserModel))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponseModel))]
        public IActionResult Post([FromBody] ApplicationUserModel model)
        {
            ApplicationUser user = IApplicationUserService.ToEntity(model);

            user.Senha = Context.Cryptography.Hash(model.Senha);
            user.Role = EntitiesModels.Enum.UserRoleEnum.Usuario;

            IApplicationUserService.Add(user);

            return Ok();
        }

        [SwaggerOperation(
            Summary = "Faz edição de usuario.",
            Tags = new[] { "Usuario" }
        )]
        [HttpPut]
        [ProducesResponseType(statusCode: 200, Type = typeof(ApplicationUserModel))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponseModel))]
        public IActionResult Put([FromBody] ApplicationUserModel model)
        {
            var user = IApplicationUserService.Find(model.Id);

            if (user != null)
            {

                if (!string.IsNullOrEmpty(model.Login))
                    user.Login = model.Login;
                if (!string.IsNullOrEmpty(model.Email))
                    user.Email = model.Email;
                if(!string.IsNullOrEmpty(model.Nome))
                    user.Nome = model.Nome;
                if (!string.IsNullOrEmpty(model.Senha))
                    user.Senha = Context.Cryptography.Hash(model.Senha);


                IApplicationUserService.Edit(user);
            } else
            {
                return NotFound();
            }

            return Ok();
        }
    }
}
