﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Desafio_ioasys.Entities;
using Desafio_ioasys.Models;
using Desafio_ioasys.Repositories;
using Desafio_ioasys.Repositories.Interfaces;
using Desafio_ioasys.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Desafio_ioasys.Controllers
{

    [ApiController]
    [ApiVersion("1")]
    [Authorize("Bearer")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class FilmesController : Controller
    {

        private IFilmeService IFilmeService;
        private IApplicationUserService IApplicationUserService;

        public FilmesController(IFilmeService service, IApplicationUserService userService)
        {
            IFilmeService = service;
            IApplicationUserService = userService;
        }

        [SwaggerOperation(
            Summary = "Recupera uma coleção dos filmes.",
            Tags = new[] { "Filme" }
        )]
        [HttpGet]
        [ProducesResponseType(statusCode: 200, Type = typeof(List<Filme>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponseModel))]
        [Route("")]
        [Route("{id?}")]
        public IActionResult Get(int? id)
        {
            var lista = IFilmeService.List().Where(f => (!id.HasValue || id.Value == f.Id)).ToList();
            List<FilmeModel> filmes = new List<FilmeModel>();
            foreach (var item in lista)
            {
                filmes.Add(IFilmeService.ToModel(item));
            }
            return Ok(filmes);
        }

        [SwaggerOperation(
            Summary = "Recupera uma coleção dos filmes com filtros para seleção.",
            Tags = new[] { "Filme" }
        )]
        [HttpGet]
        [ProducesResponseType(statusCode: 200, Type = typeof(List<FilmeModel>))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponseModel))]
        [Route("filtros")]
        public IActionResult Get([FromBody] FilmeModel model)
        {
            var lista = IFilmeService.List().ToList();

            if (!string.IsNullOrEmpty(model.Diretor))
                lista = lista.Where(f => f.Diretor.ToUpper().StartsWith(model.Diretor.ToUpper())).ToList();

            if (model.Genero >= 0)
                lista = lista.Where(f => f.Genero == (Entities.Enum.GeneroEnum)model.Genero).ToList();

            if (!string.IsNullOrEmpty(model.Nome))
                lista = lista.Where(f => f.Nome.ToUpper().StartsWith(model.Nome.ToUpper())).ToList();

            if (!string.IsNullOrEmpty(model.NomeOriginal))
                lista = lista.Where(f => f.NomeOriginal.ToUpper().StartsWith(model.NomeOriginal.ToUpper())).ToList();

            List<FilmeModel> filmes = new List<FilmeModel>();
            foreach (var item in lista)
            {
                filmes.Add(IFilmeService.ToModel(item));
            }
            return Ok(filmes);
        }


        [SwaggerOperation(
            Summary = "Faz inclusão de filme.",
            Tags = new[] { "Filme" }
        )]
        [HttpPost]
        [ProducesResponseType(statusCode: 200, Type = typeof(FilmeModel))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponseModel))]
        public IActionResult Post([FromBody] FilmeModel model)
        {
            Filme f = IFilmeService.ToEntity(model);
            IFilmeService.Add(f);

            return Ok();
        }


        [SwaggerOperation(
            Summary = "Faz avaliação do filme.",
            Tags = new[] { "Filme" }
        )]
        [HttpPut]
        [ProducesResponseType(statusCode: 200, Type = typeof(FilmeAvaliacaoModel))]
        [ProducesResponseType(statusCode: 500, Type = typeof(ErroResponseModel))]
        public IActionResult Put([FromBody] FilmeAvaliacaoModel model)
        {
            Filme f = IFilmeService.Find(model.Filme);
            if (f == null)
                return NotFound();

            ApplicationUser u = IApplicationUserService.Find(model.Usuario);
            if (u == null)
                return NotFound();

            IFilmeService.Avaliar(f, u, model.Avaliacao, model.Comentario);

            return Ok();
        }
    }
}
