﻿using Desafio_ioasys.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Desafio_ioasys.Migrations
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Filme>().HasData(
                new Filme { Id = 1, Nome = "Hamlet", Genero = Entities.Enum.GeneroEnum.Comedia, Diretor = "Hamlet", DataLancamento = DateTime.Parse("2021-02-08") },
                new Filme { Id = 2, Nome = "King Lear", Genero = Entities.Enum.GeneroEnum.Documentario, Diretor = "King Lear", DataLancamento = DateTime.Parse("2021-02-02") },
                new Filme { Id = 3, Nome = "Othello", Genero = Entities.Enum.GeneroEnum.Drama, Diretor = "Othello", DataLancamento = DateTime.Parse("2021-02-01") }
            );


            modelBuilder.Entity<FilmeAvaliacao>().HasData(
                new FilmeAvaliacao { Id = 1, FilmeId = 1, UsuarioId= 51, Avaliacao=1, Comentario = "Comentario 1" },
                new FilmeAvaliacao { Id = 2, FilmeId = 1, UsuarioId = 51, Avaliacao = 1, Comentario = "Comentario 2" },
                new FilmeAvaliacao { Id = 3, FilmeId = 1, UsuarioId = 51, Avaliacao = 4, Comentario = "Comentario 3" }
            );


            modelBuilder.Entity<ApplicationUser>().HasData(
                new ApplicationUser { Id = 1, Login = "ioasys", Email = "testeapple@ioasys.com.br", Nome = "ioasys", Senha = Context.Cryptography.Hash("Uioasys12341234!"), Ativo = true, Role = EntitiesModels.Enum.UserRoleEnum.Administrador },
                new ApplicationUser { Id = 2, Login = "admin_apiprodutos", Email = "admin-apiprodutos@teste.com.br", Nome = "admin-apiprodutos", Senha = Context.Cryptography.Hash("AdminAPIProdutos01!"), Ativo = true, Role = EntitiesModels.Enum.UserRoleEnum.Administrador },
                new ApplicationUser { Id = 50, Login = "teste1", Email = "teste1@ioasys.com.br", Nome = "teste1", Senha = Context.Cryptography.Hash("teste1"), Ativo = true, Role = EntitiesModels.Enum.UserRoleEnum.Usuario },
                new ApplicationUser { Id = 51, Login = "teste2", Email = "teste2@teste.com.br", Nome = "teste2", Senha = Context.Cryptography.Hash("teste2"), Ativo = true, Role = EntitiesModels.Enum.UserRoleEnum.Usuario }
            );

        }
    }
}
