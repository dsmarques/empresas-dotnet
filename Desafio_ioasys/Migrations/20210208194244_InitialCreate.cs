﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Desafio_ioasys.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ApplicationUsers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Login = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Nome = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Senha = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Role = table.Column<int>(type: "int", nullable: false),
                    Discriminator = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Atores",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    DataNascimento = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Nacionalidade = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Atores", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Filmes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    NomeOriginal = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Genero = table.Column<int>(type: "int", nullable: false),
                    Diretor = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    DataLancamento = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Filmes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FilmesAtor",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FilmeId = table.Column<int>(type: "int", nullable: true),
                    AtorId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FilmesAtor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FilmesAtor_Atores_AtorId",
                        column: x => x.AtorId,
                        principalTable: "Atores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FilmesAtor_Filmes_FilmeId",
                        column: x => x.FilmeId,
                        principalTable: "Filmes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FilmesAvaliacao",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FilmeId = table.Column<int>(type: "int", nullable: false),
                    UsuarioId = table.Column<int>(type: "int", nullable: true),
                    Avaliacao = table.Column<short>(type: "smallint", nullable: false),
                    Comentario = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FilmesAvaliacao", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FilmesAvaliacao_ApplicationUsers_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "ApplicationUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FilmesAvaliacao_Filmes_FilmeId",
                        column: x => x.FilmeId,
                        principalTable: "Filmes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "ApplicationUsers",
                columns: new[] { "Id", "Ativo", "Discriminator", "Email", "Login", "Nome", "Role", "Senha" },
                values: new object[,]
                {
                    { 1, true, "ApplicationUser", "testeapple@ioasys.com.br", "ioasys", "ioasys", 0, "24261ecb85ad344dcfe92b97f55e5c01892c8a798f16784990c03bfc9623b524" },
                    { 2, true, "ApplicationUser", "admin-apiprodutos@teste.com.br", "admin_apiprodutos", "admin-apiprodutos", 0, "d6eb9d11e82b199479064aaf0fb5c0dae3289989b1c48cad8443ff7d052b7110" },
                    { 50, true, "ApplicationUser", "teste1@ioasys.com.br", "teste1", "teste1", 1, "15bf532d22345576b4a51b96da4754c039ef3458494066d76828e893d69ebd1e" },
                    { 51, true, "ApplicationUser", "teste2@teste.com.br", "teste2", "teste2", 1, "f91a799a01ebac97c8995f50381b5be0d43ca7eb0c6a6c6bc07603c5a922d1fa" }
                });

            migrationBuilder.InsertData(
                table: "Filmes",
                columns: new[] { "Id", "DataLancamento", "Diretor", "Genero", "Nome", "NomeOriginal" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 2, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "Hamlet", 1, "Hamlet", null },
                    { 2, new DateTime(2021, 2, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "King Lear", 3, "King Lear", null },
                    { 3, new DateTime(2021, 2, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Othello", 0, "Othello", null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_FilmesAtor_AtorId",
                table: "FilmesAtor",
                column: "AtorId");

            migrationBuilder.CreateIndex(
                name: "IX_FilmesAtor_FilmeId",
                table: "FilmesAtor",
                column: "FilmeId");

            migrationBuilder.CreateIndex(
                name: "IX_FilmesAvaliacao_FilmeId",
                table: "FilmesAvaliacao",
                column: "FilmeId");

            migrationBuilder.CreateIndex(
                name: "IX_FilmesAvaliacao_UsuarioId",
                table: "FilmesAvaliacao",
                column: "UsuarioId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FilmesAtor");

            migrationBuilder.DropTable(
                name: "FilmesAvaliacao");

            migrationBuilder.DropTable(
                name: "Atores");

            migrationBuilder.DropTable(
                name: "ApplicationUsers");

            migrationBuilder.DropTable(
                name: "Filmes");
        }
    }
}
