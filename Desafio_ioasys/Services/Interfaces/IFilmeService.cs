﻿using System;
using Desafio_ioasys.Entities;
using Desafio_ioasys.Models;

namespace Desafio_ioasys.Services.Interfaces
{
    public interface IFilmeService : IBaseService<Filme>
    {

        Filme ToEntity(FilmeModel model);

        FilmeModel ToModel(Filme obj);

        void Avaliar(Filme filme, ApplicationUser usuario, short avaliacao, string comentario);
    }
}
