﻿using System;
using System.Linq;

namespace Desafio_ioasys.Services.Interfaces
{
    public interface IBaseService<T> where T : class
    {
        T Find(int id);
        IQueryable<T> List();
        void Add(T item);
        void Remove(T item);
        void Edit(T item);
    }
}
