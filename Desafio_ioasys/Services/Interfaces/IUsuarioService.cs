﻿using System;
using Desafio_ioasys.Entities;

namespace Desafio_ioasys.Services.Interfaces
{
    public interface IUsuarioService : IBaseService<Usuario>
    {
    }
}
