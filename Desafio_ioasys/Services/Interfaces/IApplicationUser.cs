﻿using System;
using Desafio_ioasys.Entities;
using Desafio_ioasys.Models;

namespace Desafio_ioasys.Services.Interfaces
{
    public interface IApplicationUserService : IBaseService<ApplicationUser>
    {

        ApplicationUser FindByLoginOrEmail(string login);

        ApplicationUser ToEntity(ApplicationUserModel model);

        ApplicationUserModel ToModel(ApplicationUser obj);


    }
}
