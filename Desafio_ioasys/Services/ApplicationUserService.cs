﻿using System;
using System.Linq;
using Desafio_ioasys.Context;
using Desafio_ioasys.Entities;
using Desafio_ioasys.Models;
using Desafio_ioasys.Repositories.Interfaces;
using Desafio_ioasys.Services.Interfaces;

namespace Desafio_ioasys.Services
{
    public class ApplicationUserService : BaseService<ApplicationUser>, IApplicationUserService
    {
        public ApplicationUserService(ApplicationDbContext context) : base(context)
        {
        }

        public ApplicationUser FindByLoginOrEmail(string login)
        {
            return this.List().Where(t => t.Login.ToUpper().Equals(login.ToUpper()) || t.Email.ToUpper().Equals(login.ToUpper())).FirstOrDefault();
        }

        public ApplicationUser ToEntity(ApplicationUserModel model)
        {
            ApplicationUser u = new ApplicationUser
            {
                Id = model.Id,
                Login = model.Login,
                Email = model.Email,
                Nome = model.Nome,
                Ativo = model.Ativo,
                Role = (EntitiesModels.Enum.UserRoleEnum)model.Role
            };

            return u;
        }

        public ApplicationUserModel ToModel(ApplicationUser obj)
        {
            ApplicationUserModel u = new ApplicationUserModel
            {
                Id = obj.Id,
                Login = obj.Login,
                Email = obj.Email,
                Nome =  obj.Nome,
                Ativo = obj.Ativo,
                Role = (short)obj.Role
            };

            return u;
        }
    }
}
