﻿using System;
using System.Linq;
using Desafio_ioasys.Context;
using Desafio_ioasys.Repositories;
using Desafio_ioasys.Repositories.Interfaces;
using Desafio_ioasys.Services.Interfaces;

namespace Desafio_ioasys.Services
{
    public class BaseService<T> : IBaseService<T> where T : class
    {

        IBaseRepository<T> _repository;

        public BaseService(ApplicationDbContext context)
        {
            _repository = new BaseRepository<T>(context);
        }

        public T Find(int id)
        {
            return _repository.Find(id);
        }

        public IQueryable<T> List()
        {
            return _repository.List;
        }

        public void Add(T item)
        {
            _repository.Add(item);
        }

        public void Remove(T item)
        {
            _repository.Remove(item);
        }

        public void Edit(T item)
        {
            _repository.Edit(item);
        }


    }
}
