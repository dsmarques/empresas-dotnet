﻿using System;
using System.Linq;
using Desafio_ioasys.Context;
using Desafio_ioasys.Entities;
using Desafio_ioasys.Models;
using Desafio_ioasys.Repositories.Interfaces;
using Desafio_ioasys.Services.Interfaces;

namespace Desafio_ioasys.Services
{
    public class FilmeService : BaseService<Filme>, IFilmeService
    {
        public FilmeService(ApplicationDbContext context) : base(context)
        {
        }

        public void Avaliar(Filme filme, ApplicationUser usuario, short avaliacao, string comentario)
        {
            FilmeAvaliacao fa = new FilmeAvaliacao
            {
                Filme = filme,
                Usuario = usuario,
                Avaliacao = avaliacao,
                Comentario = comentario
            };

            if (filme.Avaliacoes == null)
                filme.Avaliacoes = new System.Collections.Generic.List<FilmeAvaliacao>();
            filme.Avaliacoes.Add(fa);

            this.Edit(filme);
        }

        public Filme ToEntity(FilmeModel model)
        {
            Filme f = new Filme
            {
                Id = model.Id,
                Nome = model.Nome,
                NomeOriginal = model.NomeOriginal,
                Genero = (Entities.Enum.GeneroEnum)model.Genero,
                Diretor = model.Diretor,
                DataLancamento = model.DataLancamento
            };

            return f;
        }

        public FilmeModel ToModel(Filme obj)
        {
            FilmeModel model = new FilmeModel
            {
                Id = obj.Id,
                Nome = obj.Nome,
                NomeOriginal = obj.NomeOriginal,
                Genero = (short)obj.Genero,
                Diretor = obj.Diretor,
                DataLancamento = obj.DataLancamento
            };


            if (obj.Avaliacoes != null && obj.Avaliacoes.Count > 0)
            {
                model.AvalicacaoMedia = (decimal)obj.Avaliacoes.Average(a => a.Avaliacao);
                model.Avaliacoes = new System.Collections.Generic.List<FilmeAvaliacaoModel>();
                foreach (FilmeAvaliacao fa in obj.Avaliacoes)
                {
                    model.Avaliacoes.Add(new FilmeAvaliacaoModel
                    {
                        Id = fa.Id,
                        Filme = fa.Filme.Id,
                        Usuario = fa.Usuario.Id,
                        Avaliacao = fa.Avaliacao,
                        Comentario = fa.Comentario
                    });
                }
            }
            return model;
        }
    }
}
