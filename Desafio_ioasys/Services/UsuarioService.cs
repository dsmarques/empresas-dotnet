﻿using System;
using Desafio_ioasys.Context;
using Desafio_ioasys.Entities;
using Desafio_ioasys.Repositories.Interfaces;
using Desafio_ioasys.Services.Interfaces;

namespace Desafio_ioasys.Services
{
    public class UsuarioService : BaseService<Usuario>, IUsuarioService
    {
        public UsuarioService(ApplicationDbContext context) : base(context)
        {
        }
    }
}
