﻿using System;
namespace Desafio_ioasys.Entities.Enum
{
    public enum GeneroEnum
    {
		Drama,
		Comedia,
		Ficcao,
		Documentario,
		Terror,
		Suspense
	}
}
