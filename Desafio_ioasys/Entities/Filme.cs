﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Desafio_ioasys.Entities.Enum;

namespace Desafio_ioasys.Entities
{
    public class Filme 
    {

		public int Id { get; set; }

		[Required]
		[StringLength(100)]
		public string Nome { get; set; }

		[StringLength(100)]
		public string NomeOriginal { get; set; }

		[Required]
		public GeneroEnum Genero { get; set; }

		[Required]
		[StringLength(100)]
		public string Diretor { get; set; }

		[Required]
		public DateTime DataLancamento { get; set; }

		public List<FilmeAvaliacao> Avaliacoes { get; set; }
	}
}
