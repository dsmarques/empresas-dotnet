﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Desafio_ioasys.Entities
{
    public class FilmeAvaliacao
    {

        public int Id { get; set; }

        [Required]
        public int FilmeId { get; set; }

        [Required]
        public Filme Filme { get; set; }

        [Required]
        public int UsuarioId { get; set; }

        [Required]
        public ApplicationUser Usuario { get; set; }

        [Required]
        public short Avaliacao { get; set; }

        public string Comentario { get; set; }
    }
}
