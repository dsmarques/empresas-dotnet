﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Desafio_ioasys.Entities
{
    public class FilmeAtor
    {

        public int Id { get; set; }

        [Required]
        public Filme Filme { get; set; }

        [Required]
        public Ator Ator { get; set; }
    }
}
