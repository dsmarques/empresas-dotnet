﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Desafio_ioasys.Entities
{
    public class Ator
    {

        public int Id { get; set; }

        [StringLength(100)]
        [Required]
        public string Nome { get; set; }

        public DateTime DataNascimento { get; set; }

        [StringLength(100)]
        public string Nacionalidade { get; set; }

    }
}
