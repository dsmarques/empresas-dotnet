﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Desafio_ioasys.Models
{
    public class FilmeAvaliacaoModel
    {

        public int Id { get; set; }

        public int Filme { get; set; }

        public int Usuario { get; set; }

        public short Avaliacao { get; set; }

        public string Comentario { get; set; }
    }
}
