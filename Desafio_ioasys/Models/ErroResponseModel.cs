﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Desafio_ioasys.Models
{
    public class ErroResponseModel
    {
        public int Codigo { get; set; }
        public string Mensagem { get; set; }
        public string[] Detalhes { get; set; }
        public ErroResponseModel InnerError { get; set; }

        public static ErroResponseModel From(System.Exception e)
        {
            if (e == null)
            {
                return null;
            }
            return new ErroResponseModel
            {
                Codigo = e.HResult,
                Mensagem = e.Message,
                InnerError = ErroResponseModel.From(e.InnerException)
            };
        }

        public static ErroResponseModel FromModelStateError(ModelStateDictionary modelState)
        {
            var errors = modelState.Values.SelectMany(v => v.Errors);
            return new ErroResponseModel
            {
                Codigo = 100,
                Mensagem = "Houve erro(s) na validação da requisição",
                Detalhes = errors.Select(e => e.ErrorMessage).ToArray(),
            };
        }
    }
}
