﻿using System;
using System.ComponentModel.DataAnnotations;
using Desafio_ioasys.EntitiesModels.Enum;
using Microsoft.AspNetCore.Identity;

namespace Desafio_ioasys.Models
{
    public class ApplicationUserModel
    {
        public int Id { get; set; }
        [Required]
        public string Login { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Senha { get; set; }
        [Required]
        public bool Ativo { get; set; }
        [Required]
        public short Role { get; set; }

    }
}
