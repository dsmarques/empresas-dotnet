﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Desafio_ioasys.Entities.Enum;

namespace Desafio_ioasys.Models
{
    public class FilmeModel
    {

		public int Id { get; set; }

		[Required]
		[StringLength(100)]
		public string Nome { get; set; }

		[StringLength(100)]
		public string NomeOriginal { get; set; }

		[Required]
		public short Genero { get; set; }

		[Required]
		[StringLength(100)]
		public string Diretor { get; set; }

		[Required]
		public DateTime DataLancamento { get; set; }

		public decimal AvalicacaoMedia { get; set; }

		public List<FilmeAvaliacaoModel> Avaliacoes { get; set; }

	}
}
