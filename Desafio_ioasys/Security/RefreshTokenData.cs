﻿using System;
namespace Desafio_ioasys.Security
{
    public class RefreshTokenData
    {
        public string RefreshToken { get; set; }
        public string UserID { get; set; }
    }
}
